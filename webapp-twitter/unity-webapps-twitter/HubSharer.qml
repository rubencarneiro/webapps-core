import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 1.0

Item {
    id: main
    anchors.fill: parent

    signal completed(string result, var resourceUploadCompletedCallback)

    property string fileToShare

    function _callback(accessToken,
                       userOauthTokenSecret,
                       consumerKey,
                       consumerSecretKey,
                       fileToShare,
		       message,
		       resourceUploadCompletedCallback) {
        itemComp.url = fileToShare;
        var dataUri = itemComp.toDataURI();

        var result = {
            accessToken: accessToken,
            accessTokenSecret: userOauthTokenSecret,
            consumerKey: consumerKey,
            consumerSecretKey: consumerSecretKey,
            fileToShareUri: fileToShare,
            fileToShare: dataUri.toString(),
            message: message
        };

        completed(JSON.stringify(result),
                  function(result) { main.destroy(); resourceUploadCompletedCallback(result); });
    }

    ContentItem {
        id: itemComp
    }

    Rectangle {
        anchors.fill: parent
        color: UbuntuColors.lightGrey

        Image {
            id: background
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            source: Qt.resolvedUrl("assets/background_full.png")
        }
    }

    Share {
        id: share
        anchors.fill: parent
        visible: true
        fileToShare: main.fileToShare
        callback: _callback
        provider: "twitter"
        onCanceled: {
            completed(JSON.stringify({status: "cancelled"}),
                      function(result) { uploadCompleted(result); });
            main.destroy();
        }
        onUploadCompleted: {
            /* if the upload was successful, we need to destroy the parent */
            if (success) {
                main.destroy();
            } else {
                console.error ("Upload failed");
                /* FIXME: Display a message to the user */
            }
        }
    }
}
