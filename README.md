Core Webapps
============

This package provides a set of default web application versions for popular
web services.

The debian package itself serves 2 purposes:
- generate click packages in a clean environment, that can be integrated with
the rest of our toolchain
- package a set of icons for the webapps, while click doesn't support icons
directly (temporary)

Building the click packages
===========================

To generate the click packages directly, ie without the debian package::

  $ ./build.sh

Running the user acceptance tests
=================================

Run tests locally
-----------------

The tests need an updated selenium package and the oxide webdriver::

    $ sudo add-apt-repository ppa:canonical-platform-qa/selenium
    $ sudo apt-get update
    $ sudo apt-get install python3-selenium oxideqt-chromedriver

To run the user acceptance tests from the source branch::

    $ cd webapp-amazon/tests/autopilot
    $ autopilot3 run --config from_source amazon_webapp

Run tests in the phone
----------------------

First flash the phone with developer mode enabled::

    $ ubuntu-device-flash --developer-mode --password 0000 \
      --channel="ubuntu-touch/devel-proposed" --wipe

When the phone is flashed, complete the welcome wizard.

To run the user accceptance tests into an Ubuntu phone with a read-only image::

    $ adt-run --click-source webapp-amazon/ \
      --click build/com.ubuntu.developer.webapps.webapp-amazon_*_all.click \
      -o /tmp/adt-webapps-tests \
      --setup-commands \
      'mount -o remount,rw /' \
      --setup-commands \
      'apt-add-repository -y ppa:canonical-platform-qa/selenium' \
      --setup-commands \
      'apt-get --no-list-cleanup update -o Dir::Etc::SourceList=/dev/null' \
      --setup-commands \
      'sync; sleep 2; mount -o remount,ro /' \
      --- ssh -s adb -- -p 0000

Run tests in a virtual machine
------------------------------

The user acceptance tests also can be run against a built and installed click
package, under qemu. To do so, install autopkgtest and qemu::

    $ sudo apt-get install autopkgtest qemu

After installing autopkgtest and qemu, you need to build an image for qemu
to use. We use vivid here, as building an image to closely resemble the actual
stable phone images is quite difficult. When this is easier in the future, we
will switch to using stable phone images for this. We output the image to ~/
rather than the current directory, so it will be in a safer place to avoid
rebuilding images every time. You can store it in any directory you wish.

::

    $ adt-buildvm-ubuntu-cloud -r vivid -a amd64 -o ~/

Then the tests may be run using adt-run with the qemu virtualization host.
The output directory option here can be wherever you like, and is where the
test artifacts will be placed. The ordering of the arguments to adt-run is
important so try to keep them in this order.

::

    $ adt-run --click-source webapp-amazon/ \
      --click build/com.ubuntu.developer.webapps.webapp-amazon_*_all.click \
      -o /tmp/adt-webapps-tests \
      --setup-commands ubuntu-touch-session \
      --setup-commands \
      'add-apt-repository -y ppa:canonical-platform-qa/selenium' \
      -U --apt-pocket proposed \
      --- qemu ~/adt-vivid-amd64-cloud.img

See the detailed results
------------------------

To examine the test results, which are in subunit format, additional tools are
required::

    $ sudo add-apt-repository ppa:thomir/trv
    $ sudo apt-get update
    $ sudo apt-get install trv
    $ trv /tmp/adt-webapps-tests/artifacts/autopilot.subunit
