/*
 * Copyright (C) 2016 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

Item {
    id: main
    anchors.fill: parent

    signal completed(string result, var resourceUploadCompletedCallback)

    property string contentType
    property string fileToShare
    property string text

    function _callback(accessToken,
                       fileToShare,
		       message,
		       resourceUploadCompletedCallback) {
        var dataUri = fileToShare;
        switch (main.contentType) {
          case 'Pictures':
            itemComp.url = fileToShare;
            dataUri = itemComp.toDataURI();
            break;
        }

        var result = {
            accessToken: accessToken,
            fileToShare: dataUri.toString(),
            message: message
        };

        completed(
            JSON.stringify(result),
            function(result) {
                main.destroy();
                resourceUploadCompletedCallback(result);
            });
    }

    ContentItem {
        id: itemComp
    }

    Rectangle {
        anchors.fill: parent
        color: UbuntuColors.lightGrey

        Image {
            id: background
            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }
            source: Qt.resolvedUrl("assets/background_full.png")
        }
    }

    Share {
        id: share

        anchors.fill: parent
        visible: true

        fileToShare: main.fileToShare
        text: main.text
        contentType: main.contentType
        callback: _callback

        onCancelled: {
            completed(JSON.stringify({status: "cancelled"}),
                      function(result) { uploadCompleted(result); });
            main.destroy();
        }

        onUploadCompleted: {
            /* if the upload was successful, we need to destroy the parent */
            if (success) {
                main.destroy();
            } else {
                console.error ("Upload failed");
                /* FIXME: Display a message to the user */
            }
        }
    }
}
