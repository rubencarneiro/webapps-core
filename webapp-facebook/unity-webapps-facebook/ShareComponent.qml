/*
 * Copyright (C) 2016 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Window 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Item {
    id: shareComponent

    property string accountProviderIconName
    property string accountProviderDisplayName
    property string contentType
    property string resourceToShare
    property string text

    signal cancelled()
    signal share(string userMessage)

    objectName: "shareComponent"

    function shareSuccessful(result) {
        activitySpinner.visible = result
    }

    ActivityIndicator {
        id: activitySpinner

        anchors.centerIn: parent
        z: 1
        visible: false

        running: visible
    }

    Column {
        id: bla

        anchors.fill: parent

        spacing: units.gu(1)

        Item {
            id: serviceHeader

            anchors.left: parent.left
            anchors.right: parent.right

            anchors.topMargin: units.gu(1)
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)

            height: childrenRect.height

            ListItem.Subtitled {
                anchors {
                    left: parent.left
                    right: parent.right
                }

                iconName: accountProviderIconName
                text: accountProviderDisplayName
                subText: accountProviderDisplayName

                showDivider: false
            }
        }

        ListItem.ThinDivider {}

        Rectangle {
            id: messageArea
            objectName: "messageArea"

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: units.gu(1)
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)

            height: units.gu(30)

            color: "#f2f2f2"

            Column {
                anchors.fill: parent

                spacing: units.gu(1)

                Rectangle {
                    id: sharedResourcePreview

                    visible: contentType === 'Pictures' || contentType === 'Links'

                    color: "#f2f2f2"

                    anchors.left: parent.left
                    anchors.right: parent.right

                    anchors.margins: units.gu(1)

                    height: units.gu(15)

                    UbuntuShape {
                        id: imageSnapshot

                        anchors.verticalCenter: parent.verticalCenter

                        height: units.gu(13)
                        width: units.gu(13)

                        visible: contentType === 'Pictures'

                        source: Image {
                            source: contentType === 'Pictures' ? resourceToShare : ''
                            sourceSize.height: imageSnapshot.height
                            sourceSize.width: imageSnapshot.width
                            fillMode: Image.PreserveAspectCrop
                        }
                    }
                    Text {
                        id: linksSnapshot

                        anchors.left: parent.left
                        anchors.right: parent.right

                        anchors.verticalCenter: parent.verticalCenter

                        anchors.margins: units.gu(1)

                        visible: contentType === 'Links'

                        wrapMode: Text.NoWrap
                        text: resourceToShare
                        clip: true
                        elide: Text.ElideRight

                        font.pixelSize: FontUtils.sizeToPixels("medium")
                        font.weight: Font.Light
                    }
                }

                TextArea {
                    id: message

                    //color: "#333333"

                    anchors.left: parent.left
                    anchors.right: parent.right

                    anchors.margins: units.gu(1)

                    wrapMode: Text.Wrap

                    clip: true
                    font.pixelSize: FontUtils.sizeToPixels("medium")
                    font.weight: Font.Light
                    focus: true

                    text: shareComponent.text
                }
            }
        }

        Item {
            id: actionsBar

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.topMargin: units.gu(2)
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)

            height: childrenRect.height

            Button {
                objectName: "cancelButton"

                anchors.left: parent.left

                text: i18n.dtr("ubuntu-ui-extras", "Cancel")

                color: "#cccccc"

                width: units.gu(10)
                height: units.gu(4)

                onClicked: cancelled()
            }

            Button {
                objectName: "shareButton"

                anchors.right: parent.right
                anchors.top: parent.top

                text: i18n.dtr("ubuntu-ui-extras", "Share")
                color: "#dd4814"

                width: units.gu(10)
                height: units.gu(4)

                enabled: !activitySpinner.visible

                onClicked: {
                    if (activitySpinner.visible) {
                        return;
                    }

                    activitySpinner.visible = true;

                    share(message.text);
                }
            }
        }

        // toggle to enable including location - Not implemented yet
        /*
            UbuntuShape {
                id: useLocation
                anchors {
                    left: parent.left
                    leftMargin: units.gu(1)
                    topMargin: units.gu(1)
                }
                color: selected ? "#cccccc" : "transparent"
                property bool selected: false
                width: units.gu(4.5)
                height: units.gu(4)

                AbstractButton {
                    anchors.fill: parent
                    onClicked: parent.selected = !parent.selected
                    Image {
                        source: "assets/icon_location.png"
                        anchors.centerIn: parent
                        height: parent.height * 0.75
                        fillMode: Image.PreserveAspectFit
                        smooth: true
                    }
                }
            }

            Label {
                anchors.left: useLocation.right
                anchors.baseline: useLocation.top
                anchors.baselineOffset: units.gu(3)
                anchors.leftMargin: units.gu(1)
                text: i18n.dtr("ubuntu-ui-extras", "Include location")
                fontSize: "small"
            }
            */
        // End location toggle

    }

    states: [
        State {
            name: "landscape-with-keyborad"
            PropertyChanges {
                target: serviceHeader
                y: - serviceHeader.height
            }
            PropertyChanges {
                target: messageArea
                height: units.gu(12)
            }
        }
    ]

    state: ((Screen.orientation === Qt.LandscapeOrientation) ||
            (Screen.orientation === Qt.InvertedLandscapeOrientation)) &&
           Qt.inputMethod.visible ? "landscape-with-keyborad" : ""
}
