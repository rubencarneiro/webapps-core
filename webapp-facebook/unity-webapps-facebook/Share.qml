/*
 * Copyright (C) 2012-2013-2016 Canonical, Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Window 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Ubuntu.OnlineAccounts 0.1
import Ubuntu.OnlineAccounts.Client 0.1


Item {
    id: root
    anchors.fill: parent

    property string fileToShare
    property string text
    property string contentType
    property var callback

    readonly property string applicationId: "com.ruben.carneiro.webapps.webapp-facebook_webapp-facebook"
    readonly property string serviceType: "webapps"
    readonly property string provider: "facebook"

    property string userAccountId
    property string accessToken
    property var account

    signal accountSelected
    signal cancelled
    signal uploadCompleted(bool success)

    onUploadCompleted: {
        shareComponent.shareCompleted(success);
        if (success)
            print ("Successfully posted");
        else
            print ("Failed to post");
    }

    ShareComponent {
        id: shareComponent
        visible: false

        anchors.fill: parent

        accountProviderIconName: root.account.provider.iconName
        accountProviderDisplayName: root.account.displayName

        resourceToShare: root.fileToShare
        contentType: root.contentType
        text: root.text

        onCancelled: root.cancelled()
        onShare: callback(root.accessToken,
                          root.fileToShare,
                          userMessage,
                          uploadCompleted);
    }

    AccountServiceModel {
        id: accounts
        serviceType: root.serviceType
        provider: root.provider
        Component.onCompleted: {
            if (count == 1) {
                srv.objectHandle = get(0, "accountServiceHandle");
            }
        }
    }

    AccountService {
        id: srv
        onObjectHandleChanged: {
            root.account = srv;
            root.account.authenticate(null);
        }
        onAuthenticated: {
            root.userAccountId = accountId;
            root.accessToken = reply.AccessToken;
            shareComponent.visible = true;
            sharemenu.visible = false;
        }
    }


    /* Menu listing online accounts */
    Item {
        id: sharemenu
        anchors.fill: parent
        visible: true

        signal selected(string accountId, string token)

        Component.onCompleted: {
            visible = true;
        }
        onSelected: {
            root.userAccountId = accountId;
            root.accessToken = token;
            shareComponent.visible = true;
            sharemenu.visible = false;
        }

        Component {
            id: acctDelegate
            Item {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                AccountService {
                    id: service
                    objectHandle: accountServiceHandle
                    onAuthenticated: {
                        sharemenu.selected(accountId, reply.AccessToken);
                    }
                }

                height: childrenRect.height

                ListItem.Subtitled {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    text: service.provider.displayName
                    subText: displayName
                    iconName: service.provider.iconName
                    __iconHeight: units.gu(5)
                    __iconWidth: units.gu(5)

                    onClicked: {
                        root.account = service;
                        root.account.authenticate(null);
                    }
                }
            }
        }

        ListView {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: childrenRect.height
            interactive: false
            visible: !listView.visible
            model: accounts
            header: ListItem.Header {
                anchors {
                    left: parent.left
                    right: parent.right
                }
                text: i18n.tr("Select account")
            }
            delegate: acctDelegate
        }

        ListView {
            id: listView
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: childrenRect.height
            header: headerComponent
            model: providerModel
            visible: accounts.count === 0
            delegate: providerDelegate

            Component {
                id: headerComponent
                ListItem.Header {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    text: i18n.tr("No online accounts configured")
                }
            }

            Component {
                id: providerDelegate
                Item {
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: childrenRect.height
                    ListItem.Standard {
                        text: model.displayName
                        iconName: model.iconName
                        onClicked: {
                            setup.providerId = providerId
                            setup.exec()
                        }
                    }
                }
            }


            ProviderModel {
                id: providerModel
                applicationId: applicationId
            }

            Setup {
                id: setup
                applicationId: applicationId
            }
        }
    }
}
