#!/bin/sh
#

rm -rf build
mkdir -p build
cd build

for i in `ls -d ../webapp-*`; do
  click build $i
done
